# Lazarus checkers

## Overview

Delphi Checkers program by Ivan Maklyakov, ported to Lazarus by Roland Chastain.

## Homepage

https://gitlab.com/rchastain/lazarus-checkers

## Screenshot

![alt text](screenshot/screenshot.png)
