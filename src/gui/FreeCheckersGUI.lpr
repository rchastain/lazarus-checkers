program FreeCheckersGUI;

{$MODE Delphi}

uses
  Forms,
  Interfaces,
  frmMain in 'frmMain.pas' {Form1},
  frmOut in 'frmOut.pas' {Form2},
  frmAbout in 'frmAbout.pas' {Form3},
  frmNew in 'frmNew.pas' {Form4},
  DefaultTranslator;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TForm3, Form3);
  Application.CreateForm(TForm4, Form4);
  Application.Run;
end.
