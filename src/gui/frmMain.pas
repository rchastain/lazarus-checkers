unit frmMain;

{$MODE Delphi}

interface

uses
  LCLIntf, LCLType, SysUtils, Variants, Classes,
  Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus,
  StdCtrls, frmOut, frmAbout, frmNew, Buttons,
  vars, position, generator, move, utils, think, hash;

type
  TForm1 = class(TForm)
    Pole: TImage;
    ImgFrom: TImage;
    Fig0: TImage;
    Fig1: TImage;
    Fig2: TImage;
    Fig5: TImage;
    Fig6: TImage;
    Timer1: TTimer;
    Memo1: TMemo;
    Memo2: TMemo;
    Timer2: TTimer;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    BitBtn8: TBitBtn;
    ListBox1: TListBox;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    BitBtn11: TBitBtn;
    BitBtn12: TBitBtn;
    BitBtn13: TBitBtn;
    BitBtn14: TBitBtn;
    LastFrom: TImage;
    LastTo: TImage;
    Cap1: TImage;
    Cap2: TImage;
    Cap3: TImage;
    Cap4: TImage;
    Cap5: TImage;
    Cap6: TImage;
    Cap7: TImage;
    Cap8: TImage;
    Cap9: TImage;
    Cap10: TImage;
    Cap11: TImage;
    Cap12: TImage;
    Image5: TImage;
    Image6: TImage;
    Image7: TImage;
    Image8: TImage;
    Image12: TImage;
    Image11: TImage;
    Image10: TImage;
    Image9: TImage;
    Image14: TImage;
    Image15: TImage;
    Image16: TImage;
    Image17: TImage;
    Image21: TImage;
    Image20: TImage;
    Image19: TImage;
    Image18: TImage;
    Image23: TImage;
    Image24: TImage;
    Image25: TImage;
    Image26: TImage;
    Image30: TImage;
    Image29: TImage;
    Image28: TImage;
    Image27: TImage;
    Image32: TImage;
    Image33: TImage;
    Image34: TImage;
    Image35: TImage;
    Image39: TImage;
    Image38: TImage;
    Image37: TImage;
    Image36: TImage;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn13Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
    procedure BitBtn14Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Image5MouseMove(Sender: TObject; Shift: TShiftState; X, Y: integer);
    procedure PoleMouseMove(Sender: TObject; Shift: TShiftState; X, Y: integer);
    procedure Image5Click(Sender: TObject);
  private
    { Private declarations }
    procedure New;
    procedure Show;
    procedure Clear_vid;
    procedure Show_Cap(new_cap: byte);
    procedure Move_Make(mov: tmove);
    procedure ShowTimer;
    procedure load_open;
    procedure save_game(nam: string);
    procedure autosave;
  public
    { Public declarations }
  end;

  tvid = record
    m_from: byte;
    m_to: array[1..32] of byte;
    move_num: array[1..32] of byte;
    to_num: byte;
  end;

  tgame = record
    pos: string;
    mov: tmove;
    b_tim, w_tim: longint;
  end;

var
  Form1: TForm1;
  lol_board: tboard;
  norm_board: boolean;
  vid: tvid;
  cap_to: byte;
  w_timer, b_timer: longint;
  btm: byte;
  game: array[1..1024] of tgame;
  move_current, move_total: integer;
  stpos: string;

implementation

{$R *.lfm}

resourcestring
  RSGame = 'Партия:';

procedure tform1.autosave;
var
  s, ss: string;
  i: integer;
begin
  s := curdir + '/games/autosave';
  for i := 0 to 999 do
  begin
    ss := s;
    if i < 100 then
      ss := ss + '0';
    if i < 10 then
      ss := ss + '0';
    ss := ss + IntToStr(i) + '.fcs';
    if not (fileexists(ss)) then
      break;
  end;
  save_game(ss);
end;

procedure tform1.save_game(nam: string);
var
  i: integer;
  f: textfile;
begin
  assignfile(f, nam);
  rewrite(f);
  writeln(f, ver);
  writeln(f, stpos);
  writeln(f, user_name);
  writeln(f, ctm);
  writeln(f, rules_type);
  writeln(f, rules_tim);
  writeln(f, rules_ply);
  writeln(f, ctm);
  writeln(f, move_total);
  for i := 1 to move_total do
    writeln(f, movetostr(game[i].mov));
  closefile(f);
end;

procedure tform1.load_open;
var
  s, ss, tt: string;
  m_from, m_to, i, j, st: integer;
  temp: byte;
begin
  ss := opens[1 + random(461)];
  temp := 0;
  repeat
    s := substr(ss, ' ');
    if temp = 0 then
    begin
      if s = '.' then
        temp := 2;
      if length(s) = 3 then
        temp := 1;
      if length(s) = 4 then
        temp := 2;
    end;
    if temp = 1 then
    begin
      if length(s) = 3 then
        tt := s[2] + s[3]
      else
        tt := s[3] + s[4];
      for m_to := 0 to 44 do
        if uppercase(koord[m_to]) = uppercase(tt) then
          break;
      if m_to > 39 then
        halt;
      st := 1 + Ord(uppercase(s)[1]) - Ord('A');
      allgen(0);
      for i := 1 to moves_num[0] do
        for j := 1 to 4 do
          if (stolb[st, j] = moves[i].m_from) and (m_to = moves[i].m_to) then
          begin
            move_do(moves[i]);
            break;
          end;
    end;
    if temp = 2 then
    begin
      if s = '.' then
        wtm := wtm xor 1
      else
      begin
        tt := s[1] + s[2];
        for m_from := 0 to 44 do
          if uppercase(koord[m_from]) = uppercase(tt) then
            break;
        if m_from > 39 then
          halt;
        tt := s[3] + s[4];
        for m_to := 0 to 44 do
          if uppercase(koord[m_to]) = uppercase(tt) then
            break;
        if m_to > 39 then
          halt;
        board[m_to] := board[m_from];
        board[m_from] := 0;
        wtm := wtm xor 1;
      end;
    end;
  until ss = '';
  boardkey := GetPositionHash;
end;

procedure tform1.ShowTimer;
var
  s: string;
  timm: longint;
  hh, mm, ss: byte;
begin
  timm := abs(w_timer);
  hh := trunc(timm / (60 * 60 * 1000));
  mm := trunc((timm - hh * 60 * 60 * 1000) / (60 * 1000));
  ss := trunc((timm - hh * 60 * 60 * 1000 - mm * 60 * 1000) / 1000);
  s := '';
  if w_timer < 0 then
    s := '-';
  if hh < 10 then
    s := s + '0';
  s := s + IntToStr(hh) + ':';
  if mm < 10 then
    s := s + '0';
  s := s + IntToStr(mm) + ':';
  if ss < 10 then
    s := s + '0';
  s := s + IntToStr(ss);
  memo1.Text := s;
  timm := abs(b_timer);
  hh := trunc(timm / (60 * 60 * 1000));
  mm := trunc((timm - hh * 60 * 60 * 1000) / (60 * 1000));
  ss := trunc((timm - hh * 60 * 60 * 1000 - mm * 60 * 1000) / 1000);
  s := '';
  if b_timer < 0 then
    s := '-';
  if hh < 10 then
    s := s + '0';
  s := s + IntToStr(hh) + ':';
  if mm < 10 then
    s := s + '0';
  s := s + IntToStr(mm) + ':';
  if ss < 10 then
    s := s + '0';
  s := s + IntToStr(ss);
  memo2.Text := s;
end;

procedure tform1.Show_Cap(new_cap: byte);
var
  i, a, x, y: integer;
  mov: tmove;
begin
  if (vid.m_from = 0) or (new_cap = cap_to) then
    exit;
  cap_to := new_cap;
  for i := 1 to 12 do
    timage(findcomponent('Cap' + IntToStr(i))).Visible := False;
  mov.m_from := 0;
  for i := 1 to vid.to_num do
    if new_cap = vid.m_to[i] then
    begin
      if mov.m_from <> 0 then
        if moves[vid.move_num[i]].cap_num <= mov.cap_num then
          continue;
      mov := moves[vid.move_num[i]];
    end;
  if mov.m_from = 0 then
    exit;
  for i := 1 to mov.cap_num do
  begin
    a := mov.cap_sq[i];
    if not (Norm_Board) then
      a := 44 - a;
    a := square64[a];
    x := (a - 1) mod 8;
    y := (a - 1) div 8;
    timage(findcomponent('Cap' + IntToStr(i))).Top := 8 + y * 48;
    timage(findcomponent('Cap' + IntToStr(i))).Left := 8 + x * 48;
    timage(findcomponent('Cap' + IntToStr(i))).Visible := True;
  end;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var
  mov: tmove;
begin
  if ctm = wtm then
  begin
    form2.StringGrid1.RowCount := 2;
    form2.StringGrid1.Cells[0, 1] := '';
    form2.StringGrid1.Cells[1, 1] := '';
    form2.StringGrid1.Cells[2, 1] := '';
    form2.StringGrid1.Cells[3, 1] := '';
    form2.StringGrid1.Cells[4, 1] := '';
    form2.StringGrid1.Cells[5, 1] := '';
    timer1.Enabled := False;
    listbox1.Enabled := False;
    BitBtn1.Enabled := False;
    BitBtn2.Enabled := False;
    BitBtn3.Enabled := False;
    BitBtn4.Enabled := False;
    BitBtn5.Enabled := False;
    BitBtn6.Enabled := False;
    BitBtn7.Enabled := False;
    BitBtn8.Enabled := False;
    BitBtn10.Enabled := False;
    BitBtn11.Enabled := False;
    BitBtn13.Enabled := False;
    analiz := False;
    if btm = 0 then
      Time_all := w_timer
    else
      Time_all := b_timer;
    mov := root;
    Move_Make(mov);
    listbox1.Enabled := full_mode;
    BitBtn1.Enabled := True;
    BitBtn2.Enabled := full_mode;
    BitBtn3.Enabled := full_mode;
    BitBtn4.Enabled := full_mode;
    BitBtn5.Enabled := full_mode;
    BitBtn6.Enabled := full_mode;
    BitBtn7.Enabled := full_mode;
    BitBtn8.Enabled := full_mode;
    BitBtn10.Enabled := full_mode;
    BitBtn11.Enabled := full_mode;
    BitBtn13.Enabled := True;
    timer1.Enabled := True;
  end;
end;

procedure TForm1.Timer2Timer(Sender: TObject);
var
  timm: longint;
begin
  timm := gettimer2;
  if btm = 0 then
  begin
    if rules_type = 1 then
      Dec(w_timer, timm)
    else
      Inc(w_timer, timm);
  end
  else
  begin
    if rules_type = 1 then
      Dec(b_timer, timm)
    else
      Inc(b_timer, timm);
  end;
  starttimer2;
  showtimer;
end;

procedure tform1.Move_Make(mov: tmove);
var
  s: string;
  i: integer;
begin
  move_do(mov);
  Inc(move_current);
  if (move_current mod 2) = 0 then
    s := '       '
  else
    s := IntToStr(1 + (move_current div 2)) + '. ';
  s := s + move3str(mov);
  if movetostr(game[move_current].mov) <> movetostr(mov) then
  begin
    for i := move_total downto move_current do
      listbox1.Items.Delete(i);
    move_total := move_current;
  end;
  listbox1.Items.Add(s);
  listbox1.ItemIndex := move_current;
  game[move_current + 1].pos := postostr;
  game[move_current].mov := mov;
  game[move_current + 1].b_tim := b_timer;
  game[move_current + 1].w_tim := w_timer;
  Allgen(0);
  Clear_vid;
  if moves_num[0] = 0 then
  begin
    if wtm = 0 then
      ShowMessage('Черные победили')
    else
      ShowMessage('Белые победили');
    timer1.Enabled := False;
    timer2.Enabled := False;
  end;
  starttimer2;
  btm := wtm;
  timer2.Enabled := True;
end;

procedure TForm1.BitBtn10Click(Sender: TObject);
var
  mov: tmove;
begin
  form2.Show;
  analiz := True;
  form2.StringGrid1.RowCount := 2;
  form2.StringGrid1.Cells[0, 1] := '';
  form2.StringGrid1.Cells[1, 1] := '';
  form2.StringGrid1.Cells[2, 1] := '';
  form2.StringGrid1.Cells[3, 1] := '';
  form2.StringGrid1.Cells[4, 1] := '';
  form2.StringGrid1.Cells[5, 1] := '';
  timer1.Enabled := False;
  listbox1.Enabled := False;
  BitBtn1.Enabled := False;
  BitBtn2.Enabled := False;
  BitBtn3.Enabled := False;
  BitBtn4.Enabled := False;
  BitBtn5.Enabled := False;
  BitBtn6.Enabled := False;
  BitBtn7.Enabled := False;
  BitBtn8.Enabled := False;
  BitBtn10.Enabled := False;
  BitBtn11.Enabled := False;
  BitBtn13.Enabled := False;
  if btm = 0 then
    Time_all := w_timer
  else
    Time_all := b_timer;
  mov := root;
  listbox1.Enabled := full_mode;
  BitBtn2.Enabled := True;
  BitBtn2.Enabled := full_mode;
  BitBtn3.Enabled := full_mode;
  BitBtn4.Enabled := full_mode;
  BitBtn5.Enabled := full_mode;
  BitBtn6.Enabled := full_mode;
  BitBtn7.Enabled := full_mode;
  BitBtn8.Enabled := full_mode;
  BitBtn10.Enabled := full_mode;
  BitBtn11.Enabled := full_mode;
  BitBtn13.Enabled := True;
  timer1.Enabled := True;
end;

procedure TForm1.BitBtn11Click(Sender: TObject);
begin
  ctm := wtm;
end;

procedure TForm1.BitBtn12Click(Sender: TObject);
begin
  stop := True;
end;

procedure TForm1.BitBtn13Click(Sender: TObject);
begin
  form3.ShowModal;
end;

procedure TForm1.BitBtn14Click(Sender: TObject);
begin
  stop := True;
  halt;
end;

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
  form2.StringGrid1.RowCount := 2;
  form2.StringGrid1.Cells[0, 1] := '';
  form2.StringGrid1.Cells[1, 1] := '';
  form2.StringGrid1.Cells[2, 1] := '';
  form2.StringGrid1.Cells[3, 1] := '';
  form2.StringGrid1.Cells[4, 1] := '';
  form2.StringGrid1.Cells[5, 1] := '';
  New;
end;

procedure TForm1.BitBtn4Click(Sender: TObject);
var
  i: integer;
begin
  Norm_Board := not (Norm_Board);
  for i := 1 to 32 do
    lol_board[map[i]] := 255;
  Show;
end;

procedure TForm1.BitBtn9Click(Sender: TObject);
begin
  form2.Show;
  form1.SetFocus;
end;

procedure tform1.Clear_vid;
var
  i: integer;
begin
  Show_Cap(0);
  ImgFrom.Visible := False;
  if vid.m_from <> 0 then
  begin
    lol_board[vid.m_from] := 255;
    for i := 1 to vid.to_num do
      lol_board[vid.m_to[i]] := 255;
  end;
  vid.m_from := 0;
  vid.to_num := 0;
  Show;
end;

procedure tform1.Show;
var
  i, a, x, y: integer;
begin
  for i := 5 to 39 do
    if board[i] <> lol_board[i] then
    begin
      if norm_board then
        timage(findcomponent('Image' + IntToStr(i))).Picture :=
          timage(findcomponent('Fig' + IntToStr(board[i]))).Picture
      else
        timage(findcomponent('Image' + IntToStr(44 - i))).Picture :=
          timage(findcomponent('Fig' + IntToStr(board[i]))).Picture;
      lol_board[i] := board[i];
    end;
  if move_current = 0 then
  begin
    lastfrom.Visible := False;
    lastto.Visible := False;
  end
  else
  begin
    if norm_board then
      a := square64[game[move_current].mov.m_from]
    else
      a := square64[44 - game[move_current].mov.m_from];
    x := (a - 1) mod 8;
    y := (a - 1) div 8;
    lastfrom.Top := 8 + y * 48;
    lastfrom.Left := 8 + x * 48;
    lastfrom.Visible := True;
    if norm_board then
      a := square64[game[move_current].mov.m_to]
    else
      a := square64[44 - game[move_current].mov.m_to];
    x := (a - 1) mod 8;
    y := (a - 1) div 8;
    lastto.Top := 8 + y * 48;
    lastto.Left := 8 + x * 48;
    lastto.Visible := True;
  end;
  if vid.m_from <> 0 then
  begin
    if norm_board then
      a := square64[vid.m_from]
    else
      a := square64[44 - vid.m_from];
    x := (a - 1) mod 8;
    y := (a - 1) div 8;
    ImgFrom.Top := 8 + y * 48;
    ImgFrom.Left := 8 + x * 48;
    ImgFrom.Visible := True;
    for i := 1 to vid.to_num do
      if vid.m_to[i] <> vid.m_from then
        if norm_board then
          timage(findcomponent('Image' + IntToStr(vid.m_to[i]))).Picture :=
            ImgFrom.Picture
        else
          timage(findcomponent('Image' + IntToStr(44 - vid.m_to[i]))).Picture :=
            ImgFrom.Picture;
  end;
end;

procedure tform1.New;
var
  i: integer;
begin
  random_open := False;
  i := form4.ShowModal;
  if i = mrCancel then
  begin
    if Caption = '' then
      halt;
    exit;
  end;
  if move_total <> 0 then
    autosave;
  newgame;
  if random_open then
    load_open;
  stpos := postostr;
  //strtopos('....B....................b.W....b');
  Caption := ver + ' GUI';
  //Caption := Caption + ' играет ' + user_name;
  listbox1.Enabled := full_mode;
  BitBtn2.Enabled := full_mode;
  BitBtn3.Enabled := full_mode;
  BitBtn4.Enabled := full_mode;
  BitBtn5.Enabled := full_mode;
  BitBtn6.Enabled := full_mode;
  BitBtn7.Enabled := full_mode;
  BitBtn8.Enabled := full_mode;
  BitBtn9.Enabled := full_mode;
  BitBtn10.Enabled := full_mode;
  BitBtn11.Enabled := full_mode;
  BitBtn12.Enabled := full_mode;
  for i := 0 to 44 do
    lol_board[i] := 255;
  lol_board[0] := 128;
  lol_board[1] := 128;
  lol_board[2] := 128;
  lol_board[3] := 128;
  lol_board[4] := 128;
  lol_board[13] := 128;
  lol_board[22] := 128;
  lol_board[31] := 128;
  lol_board[40] := 128;
  lol_board[41] := 128;
  lol_board[42] := 128;
  lol_board[43] := 128;
  lol_board[44] := 128;
  norm_board := ctm <> 0;
  vid.m_from := 0;
  vid.to_num := 0;
  cap_to := 0;
  Clear_vid;
  move_current := 0;
  move_total := 0;
  Show;
  AllGen(0);
  w_timer := 0;
  b_timer := 0;
  if rules_type = 1 then
  begin
    w_timer := 60000 * rules_tim;
    b_timer := 60000 * rules_tim;
    rules_ply := maxply;
  end;
  listbox1.Items.Clear;
  listbox1.Items.Add({'Партия:'}RSGame);
  listbox1.ItemIndex := 0;
  timer2.Enabled := False;
  dumaem := False;
  game[1].pos := startpos;
  game[1].mov.m_from := 0;
  game[1].mov.m_to := 0;
  game[1].b_tim := b_timer;
  game[1].w_tim := w_timer;
  showtimer;
end;

procedure TForm1.PoleMouseMove(Sender: TObject; Shift: TShiftState; X, Y: integer);
begin
  if wtm = ctm then
    exit;
  Show_Cap(0);
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  i: integer;
  f: textfile;
begin
  randomize;
  assignfile(f, 'fly.dat');
  reset(f);
  for i := 1 to 461 do
    readln(f, opens[i]);
  closefile(f);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  if move_total <> 0 then
    autosave;
end;

procedure TForm1.FormShow(Sender: TObject);
var
  i: integer;
begin
  curdir := getcurrentdir;
  pole.Picture.LoadFromFile(curdir + '/img/pole.bmp');
  imgfrom.Picture.LoadFromFile(curdir + '/img/v1.bmp');
  cap1.Picture.LoadFromFile(curdir + '/img/v2.bmp');
  fig0.Picture.LoadFromFile(curdir + '/img/pp.bmp');
  fig1.Picture.LoadFromFile(curdir + '/img/wm.bmp');
  fig2.Picture.LoadFromFile(curdir + '/img/bm.bmp');
  fig5.Picture.LoadFromFile(curdir + '/img/wk.bmp');
  fig6.Picture.LoadFromFile(curdir + '/img/bk.bmp');
  lastfrom.Picture.LoadFromFile(curdir + '/img/v3.bmp');
  lastto.Picture := lastfrom.Picture;
  full_mode := fileexists(curdir + '/img/убейсибяапстену');
  ctm := 1;
  HashSizeMB := 16;
  FillZobristTables;
  HashInit;
  for i := 2 to 12 do
    timage(findcomponent('Cap' + IntToStr(i))).Picture := Cap1.Picture;
  new;
end;

procedure TForm1.Image5Click(Sender: TObject);
var
  a, b, i: integer;
  mov: tmove;
begin
  if dumaem then
    exit;
  if wtm = ctm then
    exit;
  a := timage(Sender).Tag;
  b := a;
  if not (norm_board) then
    b := 44 - b;
  if (b <> vid.m_from) and ((((board[b] and white) <> 0) and (wtm = 0)) or
    (((board[b] and black) <> 0) and (wtm = 1))) then
  begin
    if vid.m_from <> b then
    begin
      Clear_vid;
      vid.m_from := b;
      vid.to_num := 0;
      for i := 1 to moves_num[0] do
        if moves[i].m_from = b then
        begin
          Inc(vid.to_num);
          vid.m_to[vid.to_num] := moves[i].m_to;
          vid.move_num[vid.to_num] := i;
        end;
      Show;
    end;
  end
  else if vid.m_from <> 0 then
  begin
    mov.m_from := 0;
    for i := 1 to vid.to_num do
      if b = vid.m_to[i] then
      begin
        if mov.m_from <> 0 then
          if moves[vid.move_num[i]].cap_num <= mov.cap_num then
            continue;
        mov := moves[vid.move_num[i]];
      end;
    if mov.m_from = 0 then
    begin
      beep;
      exit;
    end;
    move_make(mov);
  end;
end;

procedure TForm1.Image5MouseMove(Sender: TObject; Shift: TShiftState; X, Y: integer);
var
  a: integer;
begin
  if wtm = ctm then
    exit;
  a := timage(Sender).Tag;
  if not (norm_board) then
    a := 44 - a;
  show_cap(a);
end;

end.
