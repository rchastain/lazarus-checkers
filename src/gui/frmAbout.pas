unit frmAbout;

{$MODE Delphi}

interface

uses
  LCLIntf, LCLType, LMessages, Messages, SysUtils, Variants, Classes,
  Graphics, Controls, Forms,
  Dialogs, StdCtrls, vars, Buttons;

type
  TForm3 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    BitBtn1: TBitBtn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.lfm}

procedure TForm3.FormShow(Sender: TObject);
begin
  label1.Caption := ver + ' GUI';
  if not (full_mode) then
    label1.Caption := label1.Caption + ' demo!';
end;

end.
