unit frmOut;

{$MODE Delphi}

interface

uses
  LCLIntf, LCLType, LMessages, Messages, SysUtils, Variants, Classes,
  Graphics, Controls, Forms,
  Dialogs, Grids;

type
  TForm2 = class(TForm)
    StringGrid1: TStringGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.lfm}

procedure TForm2.FormCreate(Sender: TObject);
begin
  stringgrid1.Cells[0, 0] := 'Глубина';
  stringgrid1.Cells[1, 0] := 'Время';
  stringgrid1.Cells[2, 0] := 'Позиции';
  stringgrid1.Cells[3, 0] := 'Скорость';
  stringgrid1.Cells[4, 0] := 'Оценка';
  stringgrid1.Cells[5, 0] := 'Лучшый вариант';
end;

end.
