unit frmNew;

{$MODE Delphi}

interface

uses
  LCLIntf, LCLType, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, Buttons, vars;

type

  { TForm4 }

  TForm4 = class(TForm)
    GroupBox1: TGroupBox;
    RadioButton3: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton1: TRadioButton;
    GroupBox2: TGroupBox;
    RadioButton4: TRadioButton;
    RadioButton5: TRadioButton;
    SpinEdit1: TSpinEdit;
    SpinEdit2: TSpinEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    CheckBox1: TCheckBox;
    Label1: TLabel;
    Edit1: TEdit;
    procedure BitBtn2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RadioButton5Click(Sender: TObject);
    procedure RadioButton4Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form4: TForm4;

implementation

{$R *.lfm}

procedure TForm4.BitBtn1Click(Sender: TObject);
begin
  if radiobutton4.Checked then
    rules_type := 1
  else
    rules_type := 2;
  rules_tim := SpinEdit1.Value;
  rules_ply := SpinEdit2.Value;
  if radiobutton1.Checked then
    ctm := 1;
  if radiobutton2.Checked then
    ctm := 0;
  if radiobutton3.Checked then
  begin
    ctm := 2;
    rules_type := 2;
    rules_ply := 99;
  end;
  user_name := edit1.Text;
  random_open := checkbox1.Checked;
  modalresult := mrOk;
end;

procedure TForm4.FormShow(Sender: TObject);
begin
  edit1.SetFocus;
end;

procedure TForm4.BitBtn2Click(Sender: TObject);
begin
  modalresult := mrCancel;
end;

procedure TForm4.RadioButton4Click(Sender: TObject);
begin
  spinedit1.Enabled := True;
  spinedit2.Enabled := False;
end;

procedure TForm4.RadioButton5Click(Sender: TObject);
begin
  spinedit1.Enabled := False;
  spinedit2.Enabled := True;
end;

end.
