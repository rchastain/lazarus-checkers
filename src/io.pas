unit io;

{$MODE Delphi}

interface

uses SysUtils, vars, utils, Forms;

procedure printinfo(depth, Value: integer; line: tline);
procedure updinfo;

implementation

uses frmOut;

procedure updinfo;
begin
  application.ProcessMessages;
end;

procedure printinfo(depth, Value: integer; line: tline);
var
  NPS, i: longint;
  s: string;
begin
  NPS := 0;
  tim := GetTimer1;
  if tim <> 0 then
    NPS := (nodes div tim) * 1000;
  form2.StringGrid1.RowCount := form2.StringGrid1.RowCount + 1;
  for i := form2.StringGrid1.RowCount downto 3 do
  begin
    form2.StringGrid1.Cells[0, i - 1] := form2.StringGrid1.Cells[0, i - 2];
    form2.StringGrid1.Cells[1, i - 1] := form2.StringGrid1.Cells[1, i - 2];
    form2.StringGrid1.Cells[2, i - 1] := form2.StringGrid1.Cells[2, i - 2];
    form2.StringGrid1.Cells[3, i - 1] := form2.StringGrid1.Cells[3, i - 2];
    form2.StringGrid1.Cells[4, i - 1] := form2.StringGrid1.Cells[4, i - 2];
    form2.StringGrid1.Cells[5, i - 1] := form2.StringGrid1.Cells[5, i - 2];
  end;
  form2.StringGrid1.Cells[0, 1] := IntToStr(depth);
  form2.StringGrid1.Cells[1, 1] := floattostr(tim / 1000);
  form2.StringGrid1.Cells[2, 1] := IntToStr(nodes);
  form2.StringGrid1.Cells[3, 1] := IntToStr(NPS);
  form2.StringGrid1.Cells[4, 1] := IntToStr(Value);
  s := '';
  for i := line[0].cap_num downto 1 do
    s := s + ' ' + movetostr(line[i]);
  form2.StringGrid1.Cells[5, 1] := s;
  application.ProcessMessages;
end;

end.
