unit generator;

{$MODE Delphi}

interface

uses vars, sort;

procedure AllGen(ply: integer);
procedure CapGen(ply: integer);
procedure SimpGen(ply: integer);

implementation


procedure CapGen(ply: integer);
var
  num, op: integer;

  function AddCaptured(sq: integer; var m: tmove; caps: integer): boolean;
  var
    i: integer;
  begin
    Result := False;
    for i := 1 to caps - 2 do
      if sq = m.cap_sq[i] then
        exit;
    m.cap_sq[caps + 1] := sq;
    m.cap_type[caps + 1] := board[sq];
    Result := True;
  end;

  procedure CopyMoveFromTemplate(t: tmove; caps: integer);
  var
    i: integer;
  begin
    moves[num].m_from := t.m_from;
    moves[num].prom := t.prom;
    for i := 1 to caps do
    begin
      moves[num].cap_sq[i] := t.cap_sq[i];
      moves[num].cap_type[i] := t.cap_type[i];
    end;
    moves[num].cap_num := caps;
  end;

  procedure AddKingCaptures(sq: integer; t: tmove; caps, ndir: integer);

    procedure KingCapture(sq: integer; t: tmove; caps, dir, bad_dir: integer; var found: integer);
    var
      m, mto: byte;
    begin
      if (dir = bad_dir) or (dir = -bad_dir) then
        exit;
      m := sq + dir;
      while board[m] = 0 do
        m := m + dir;
      if (board[m] and op) = 0 then
        exit;
      mto := m + dir;
      if board[mto] <> 0 then
        exit;
      if not (AddCaptured(m, t, caps)) then
        exit;
      AddKingCaptures(mto, t, caps + 1, dir);
      found := 1;
    end;

  var
    found, m, mto: integer;
  begin
    found := 0;
    m := sq;
    while board[m] = 0 do
    begin
      KingCapture(m, t, caps, -4, ndir, found);
      KingCapture(m, t, caps, -5, ndir, found);
      KingCapture(m, t, caps, 4, ndir, found);
      KingCapture(m, t, caps, 5, ndir, found);
      m := m + ndir;
    end;
    mto := m + ndir;
    if ((board[m] and op) <> 0) and (board[mto] = 0) and (AddCaptured(m, t, caps)) then
    begin
      AddKingCaptures(mto, t, caps + 1, ndir);
      exit;
    end;
    if found = 0 then
    begin
      repeat
        Inc(num);
        CopyMoveFromTemplate(t, caps);
        moves[num].m_to := sq;
        sq := sq + ndir;
      until board[sq] <> 0;
    end;
  end;

  procedure AddPromoCaptures(sq: integer; t: tmove; caps, dir: integer);
  var
    n_dir, m, mto: integer;
  begin
    case dir of
      -4: n_dir := 5;
      -5: n_dir := 4;
      4: n_dir := -5;
      5: n_dir := -4;
    end;
    m := sq + n_dir;
    while board[m] = 0 do
      m := m + n_dir;
    mto := m + n_dir;
    if ((board[m] and op) <> 0) and (board[mto] = 0) and (AddCaptured(m, t, caps)) then
    begin
      AddKingCaptures(mto, t, caps + 1, n_dir);
      exit;
    end;
    Inc(num);
    CopyMoveFromTemplate(t, caps);
    moves[num].m_to := sq;
  end;

  procedure AddManCaptures(sq: integer; t: tmove; caps, nbad_dir: integer);

    procedure ManCapture(sq: integer; t: tmove; caps, dir, bad_dir: integer; var found: integer);
    var
      m, mto: integer;
    begin
      if dir = bad_dir then
        exit;
      m := sq + dir;
      if (board[m] and op) = 0 then
        exit;
      mto := m + dir;
      if board[mto] <> 0 then
        exit;
      if not (AddCaptured(m, t, caps)) then
        exit;
      if ((mto < 9) and (wtm = 0)) or ((mto > 35) and (wtm = 1)) then
      begin
        t.prom := True;
        AddPromoCaptures(mto, t, caps + 1, dir);
        t.prom := False;
      end
      else
      begin
        t.prom := False;
        AddManCaptures(mto, t, caps + 1, -dir);
      end;
      found := 1;
    end;

  var
    found: integer;
  begin
    found := 0;
    ManCapture(sq, t, caps, -4, nbad_dir, found);
    ManCapture(sq, t, caps, -5, nbad_dir, found);
    ManCapture(sq, t, caps, 4, nbad_dir, found);
    ManCapture(sq, t, caps, 5, nbad_dir, found);
    if found = 0 then
    begin
      Inc(num);
      CopyMoveFromTemplate(t, caps);
      moves[num].m_to := sq;
    end;
  end;

  procedure TryKingCapture(sq, dir: integer);
  var
    m, mto, save: byte;
    t: tmove;
  begin
    m := sq + dir;
    while board[m] = 0 do
      m := m + dir;
    if (board[m] and op) = 0 then
      exit;
    mto := m + dir;
    if board[mto] <> 0 then
      exit;
    save := board[sq];
    board[sq] := 0;
    t.m_from := sq;
    t.prom := False;
    AddCaptured(m, t, 0);
    AddKingCaptures(mto, t, 1, dir);
    board[sq] := save;
  end;

  procedure TryManCapture(sq, dir: integer);
  var
    m, mto, save: byte;
    t: tmove;
  begin
    m := sq + dir;
    if (board[m] and op) = 0 then
      exit;
    mto := m + dir;
    if board[mto] <> 0 then
      exit;
    save := board[sq];
    board[sq] := 0;
    t.m_from := sq;
    AddCaptured(m, t, 0);
    if ((mto < 9) and (wtm = 0)) or ((mto > 35) and (wtm = 1)) then
    begin
      t.prom := True;
      AddPromoCaptures(mto, t, 1, dir);
    end
    else
    begin
      t.prom := False;
      AddManCaptures(mto, t, 1, -dir);
    end;
    board[sq] := save;
  end;

var
  i: integer;
begin
  num := ply * 100;
  if wtm = 0 then
  begin
    op := black;
    for i := 5 to 39 do
    begin
      if board[i] = white_man then
      begin
        TryManCapture(i, -4);
        TryManCapture(i, -5);
        TryManCapture(i, 4);
        TryManCapture(i, 5);
      end;
      if board[i] = white_king then
      begin
        TryKingCapture(i, -4);
        TryKingCapture(i, -5);
        TryKingCapture(i, 4);
        TryKingCapture(i, 5);
      end;
    end;
  end
  else
  begin
    op := white;
    for i := 5 to 39 do
    begin
      if board[i] = black_man then
      begin
        TryManCapture(i, -4);
        TryManCapture(i, -5);
        TryManCapture(i, 4);
        TryManCapture(i, 5);
      end;
      if board[i] = black_king then
      begin
        TryKingCapture(i, -4);
        TryKingCapture(i, -5);
        TryKingCapture(i, 4);
        TryKingCapture(i, 5);
      end;
    end;
  end;
  moves_num[ply] := num;
end;


procedure SimpGen(ply: integer);
var
  sq, mto, num: integer;
begin
  num := ply * 100;
  if wtm = 0 then
  begin
    for sq := 5 to 39 do
    begin
      if board[sq] = white_man then
      begin
        mto := sq - 5;
        if board[mto] = 0 then
        begin
          Inc(num);
          moves[num].m_from := sq;
          moves[num].m_to := mto;
          moves[num].cap_num := 0;
          moves[num].prom := mto < 9;
        end;
        mto := sq - 4;
        if board[mto] = 0 then
        begin
          Inc(num);
          moves[num].m_from := sq;
          moves[num].m_to := mto;
          moves[num].cap_num := 0;
          moves[num].prom := mto < 9;
        end;
      end;
      if board[sq] = white_king then
      begin
        mto := sq - 5;
        while board[mto] = 0 do
        begin
          Inc(num);
          moves[num].m_from := sq;
          moves[num].m_to := mto;
          moves[num].cap_num := 0;
          moves[num].prom := False;
          mto := mto - 5;
        end;
        mto := sq - 4;
        while board[mto] = 0 do
        begin
          Inc(num);
          moves[num].m_from := sq;
          moves[num].m_to := mto;
          moves[num].cap_num := 0;
          moves[num].prom := False;
          mto := mto - 4;
        end;
        mto := sq + 5;
        while board[mto] = 0 do
        begin
          Inc(num);
          moves[num].m_from := sq;
          moves[num].m_to := mto;
          moves[num].cap_num := 0;
          moves[num].prom := False;
          mto := mto + 5;
        end;
        mto := sq + 4;
        while board[mto] = 0 do
        begin
          Inc(num);
          moves[num].m_from := sq;
          moves[num].m_to := mto;
          moves[num].cap_num := 0;
          moves[num].prom := False;
          mto := mto + 4;
        end;
      end;
    end;
  end
  else
  begin
    for sq := 5 to 39 do
    begin
      if board[sq] = black_man then
      begin
        mto := sq + 5;
        if board[mto] = 0 then
        begin
          Inc(num);
          moves[num].m_from := sq;
          moves[num].m_to := mto;
          moves[num].cap_num := 0;
          moves[num].prom := mto > 35;
        end;
        mto := sq + 4;
        if board[mto] = 0 then
        begin
          Inc(num);
          moves[num].m_from := sq;
          moves[num].m_to := mto;
          moves[num].cap_num := 0;
          moves[num].prom := mto > 35;
        end;
      end;
      if board[sq] = black_king then
      begin
        mto := sq - 5;
        while board[mto] = 0 do
        begin
          Inc(num);
          moves[num].m_from := sq;
          moves[num].m_to := mto;
          moves[num].cap_num := 0;
          moves[num].prom := False;
          mto := mto - 5;
        end;
        mto := sq - 4;
        while board[mto] = 0 do
        begin
          Inc(num);
          moves[num].m_from := sq;
          moves[num].m_to := mto;
          moves[num].cap_num := 0;
          moves[num].prom := False;
          mto := mto - 4;
        end;
        mto := sq + 5;
        while board[mto] = 0 do
        begin
          Inc(num);
          moves[num].m_from := sq;
          moves[num].m_to := mto;
          moves[num].cap_num := 0;
          moves[num].prom := False;
          mto := mto + 5;
        end;
        mto := sq + 4;
        while board[mto] = 0 do
        begin
          Inc(num);
          moves[num].m_from := sq;
          moves[num].m_to := mto;
          moves[num].cap_num := 0;
          moves[num].prom := False;
          mto := mto + 4;
        end;
      end;
    end;
  end;
  moves_num[ply] := num;
end;

procedure AllGen(ply: integer);
begin
  CapGen(ply);
  if moves_num[ply] = ply * 100 then
  begin
    SimpGen(ply);
    SimpSort(ply);
  end
  else
    CapSort(ply);
end;

end.
