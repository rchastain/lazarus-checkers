unit history;

{$MODE Delphi}

interface

uses vars;

const
  HistorySize = 7350;
  HistoryMax: integer = 16384;

var
  killer: array[0..maxply, 1..2, 1..2] of byte;
  Hist: array[0..HistorySize] of integer;

procedure HistoryClear;
function HistoryScore(move: tmove): integer;
procedure GoodMove(move: tmove; depth, ply: byte);

implementation

procedure HistoryClear;
var
  i: integer;
begin
  for i := 1 to maxply do
  begin
    Killer[i, 1, 1] := 0;
    Killer[i, 2, 1] := 0;
    Killer[i, 1, 2] := 0;
    Killer[i, 2, 2] := 0;
  end;
  for i := 0 to HistorySize do
    Hist[i] := 0;
end;

function HistoryScore(move: tmove): integer;
var
  index: integer;
begin
  index := (board[move.m_from] - 1) + 6 * (move.m_from - 5) + 210 * (move.m_to - 5);
  Result := Hist[index];
end;

procedure GoodMove(move: tmove; depth, ply: byte);
var
  i, index: integer;
begin
  if move.cap_num <> 0 then
    exit;
  if (Killer[ply, 1, 1] <> move.m_from) and (Killer[ply, 1, 2] <> move.m_to) then
  begin
    Killer[ply, 2, 1] := Killer[ply, 1, 1];
    Killer[ply, 2, 2] := Killer[ply, 1, 2];
    Killer[ply, 1, 1] := move.m_from;
    Killer[ply, 1, 2] := move.m_to;
  end;
  index := (board[move.m_from] - 1) + 6 * (move.m_from - 5) + 210 * (move.m_to - 5);
  Inc(Hist[index], depth * depth);
  if Hist[index] >= HistoryMax then
    for i := 0 to HistorySize do
      Hist[i] := (Hist[i] + 1) div 2;
end;

end.
