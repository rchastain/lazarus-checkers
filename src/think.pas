unit think;

{$MODE Delphi}

interface

uses vars, move, eval, generator, utils, position, sort, history, hash, io;

function quiescence(alpha, beta, ply: integer): integer;
function fullsearch(alpha, beta, depth, ply, fracDepth: integer; pvnode: boolean;
  var line: tline): integer;
function searchroot(depth: integer; var line: tline): integer;
function root: tmove;

implementation

function root: tmove;
var
  i: integer;
  line, lastl: tline;
begin
  HistoryClear;
  dumaem := True;
  nodes := 0;
  EndTim := (abs(Time_All) div 20) + Time_Inc - 200;
  if EndTim * 2 > abs(Time_all) then
    EndTim := Time_all div 2;
  if rules_type = 2 then
    EndTim := 30720000;
  stop := False;
  AllGen(0);
  if moves_num[0] = 1 then
  begin
    Result := moves[1];
    dumaem := False;
    exit;
  end;
  starttimer1;
  for i := 1 to rules_ply do
  begin
    lastl := line;
    line[0].cap_num := 0;
    SearchRoot(i, line);
    Tim := GetTimer1;
    if (tim > (EndTim div 3)) and not (analiz) then
      stop := True;
    if stop then
    begin
      if line[0].cap_num = 0 then
        line := lastl;
      break;
    end;
    movesort(1, moves_num[0]);
  end;
  Result := line[line[0].cap_num];
  dumaem := False;
end;

function quiescence(alpha, beta, ply: integer): integer;
var
  i, best_value, Value: integer;
begin
  Inc(nodes);
  if (nodes mod 200000) = 0 then
  begin
    Tim := GetTimer1;
    if (Tim > EndTim) and not (analiz) then
    begin
      stop := True;
      Result := 0;
      exit;
    end;
    updinfo;
  end;
  if ply >= maxply then
  begin
    Value := evalute;
    if Value < -inf + 100 then
      Inc(Value, ply);
    if Value > inf - 100 then
      Dec(Value, ply);
    Result := Value;
    exit;
  end;
  CapGen(ply);
  if moves_num[ply] = ply * 100 then
  begin
    Value := evalute;
    if Value = -inf then
      Inc(Value, ply);
    if Value = inf then
      Dec(Value, ply);
    Result := Value;
    exit;
  end;
  best_value := -inf;
  for i := 100 * ply + 1 to moves_num[ply] do
  begin
    move_do(moves[i]);
    Value := -quiescence(-beta, -alpha, ply + 1);
    move_undo(moves[i]);
    if stop then
    begin
      Result := 0;
      exit;
    end;
    if Value > best_value then
      best_value := Value;
    if Value > alpha then
      alpha := Value;
    if alpha >= beta then
      break;
  end;
  Result := best_value;
end;

function fullsearch(alpha, beta, depth, ply, fracDepth: integer; pvnode: boolean;
  var line: tline): integer;
var
  i, j, best_value, Value, new_depth, hflag: integer;
  newline: tline;
  best_move: tmove;
  old_key: int64;
begin
  if depth <= 0 then
  begin
    Result := quiescence(alpha, beta, ply);
    exit;
  end;
  Inc(nodes);
  if (nodes mod 200000) = 0 then
  begin
    Tim := GetTimer1;
    if (Tim > EndTim) and not (analiz) then
    begin
      stop := True;
      Result := 0;
      exit;
    end;
    updinfo;
  end;
  HashProbe;
{  if not(pvnode) and (hashflag<>255) then
  begin
    if (ply>1) and (hashdepth>=depth) then
    begin
      case hashflag of
        hexact:
          begin
            result:=hashvalue;
            exit;
          end;
        hlower:
          begin
            if hashvalue>=beta then
            begin
              result:=hashvalue;
              exit;
            end;
          end;
        hupper:
          begin
            if hashvalue<=alpha then
            begin
              result:=hashvalue;
              exit;
            end;
          end;
      end;
    end;
  end;}
  best_value := -inf;
  hflag := hupper;
  best_move.m_from := 0;
  best_move.m_to := 0;
  AllGen(ply);
  j := moves_num[ply] - 100 * ply;
  if j = 1 then
    Inc(fracDepth, 20);
  if j = 2 then
    Inc(fracDepth, 8);
  if fracDepth >= 32 then
  begin
    Inc(depth);
    Dec(fracDepth, 32);
  end;
  old_key := boardkey;
  if j <> 0 then
    for i := 100 * ply + 1 to moves_num[ply] do
    begin
      new_depth := depth - 1;
      if moves[i].prom then
        Inc(new_depth);
      move_do(moves[i]);
      if best_value = -inf then
      begin
        newline[0].cap_num := 0;
        Value := -fullsearch(-beta, -alpha, new_depth, ply + 1, fracDepth, pvnode, newline);
        if stop then
        begin
          Result := best_value;
          move_undo(moves[i]);
          boardkey := old_key;
          exit;
        end;
      end
      else
      begin
        newline[0].cap_num := 0;
        Value := -fullsearch(-alpha - 1, -alpha, new_depth, ply + 1, fracDepth, False, newline);
        if stop then
        begin
          Result := best_value;
          move_undo(moves[i]);
          boardkey := old_key;
          exit;
        end;
        if (Value > alpha) and (Value < beta) then
        begin
          newline[0].cap_num := 0;
          Value := -fullsearch(-beta, -alpha, new_depth, ply + 1, fracDepth, pvnode, newline);
        end;
      end;
      move_undo(moves[i]);
      boardkey := old_key;
      if stop then
      begin
        Result := best_value;
        exit;
      end;
      if Value > best_value then
      begin
        best_move := moves[i];
        line := newline;
        Inc(line[0].cap_num);
        line[line[0].cap_num] := best_move;
        best_value := Value;
        if Value > alpha then
        begin
          hflag := hexact;
          alpha := Value;
          if Value >= beta then
          begin
            hflag := hlower;
            break;
          end;
        end;
      end;
    end;
  if best_move.m_from <> 0 then
    goodmove(best_move, depth, ply);
  if j = 0 then
    best_value := -inf + ply;
  if abs(best_value) < inf - 100 then
  begin
    hashmove_from := best_move.m_from;
    hashmove_to := best_move.m_to;
    HashValue := best_value;
    HashDepth := depth;
    HashFlag := hflag;
    SaveToHash;
  end;
  Result := best_value;
end;

function searchroot(depth: integer; var line: tline): integer;
var
  i, best_value, Value, fracDepth: integer;
  newline: tline;
  old_key: int64;
begin
  best_value := -inf;
  if moves[1].cap_num = 1 then
    fracDepth := 20
  else if moves[1].cap_num = 2 then
    fracDepth := 8
  else
    fracDepth := 0;
  old_key := boardkey;
  for i := 1 to moves_num[0] do
  begin
    move_do(moves[i]);
    if best_value = -inf then
    begin
      newline[0].cap_num := 0;
      Value := -fullsearch(-inf, inf, depth - 1, 1, fracDepth, True, newline);
      if stop then
      begin
        Result := best_value;
        move_undo(moves[i]);
        boardkey := old_key;
        exit;
      end;
    end
    else
    begin
      newline[0].cap_num := 0;
      Value := -fullsearch(-best_value - 1, -best_value, depth - 1, 1, fracDepth, False, newline);
      if stop then
      begin
        Result := best_value;
        move_undo(moves[i]);
        boardkey := old_key;
        exit;
      end;
      if Value > best_value then
      begin
        newline[0].cap_num := 0;
        Value := -fullsearch(-inf, -best_value, depth - 1, 1, fracDepth, True, newline);
      end;
    end;
    move_undo(moves[i]);
    boardkey := old_key;
    if stop then
    begin
      Result := best_value;
      exit;
    end;
    moves_sort[i] := Value;
    if Value > best_value then
    begin
      best_value := Value;
      line := newline;
      Inc(line[0].cap_num);
      line[line[0].cap_num] := moves[i];
      printinfo(depth, Value, line);
    end;
  end;
  Result := best_value;
end;

end.
