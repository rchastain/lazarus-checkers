unit eval;

{$MODE Delphi}

interface

uses vars;

const

  left: array[0..44] of byte = (0, 0, 0, 0, 0,
    1, 1, 0, 0,
    1, 1, 0, 0, 0,
    1, 1, 0, 0,
    1, 1, 0, 0, 0,
    1, 1, 0, 0,
    1, 1, 0, 0, 0,
    1, 1, 0, 0,
    1, 1, 0, 0, 0, 0, 0, 0, 0);

  pst_man: array[0..44] of byte = (0, 0, 0, 0, 0,
    0, 0, 0, 0,
    25, 50, 50, 50, 0,
    25, 40, 50, 20,
    15, 30, 20, 20, 0,
    15, 25, 25, 10,
    5, 10, 10, 10, 0,
    5, 5, 5, 0,
    0, 5, 15, 5, 0, 0, 0, 0, 0);

  pst_king: array[0..44] of byte = (0, 0, 0, 0, 0,
    20, 0, 10, 30,
    20, 20, 10, 30, 0,
    20, 20, 30, 10,
    0, 20, 30, 10, 0,
    10, 30, 20, 0,
    10, 30, 20, 20, 0,
    30, 10, 20, 20,
    30, 10, 0, 20, 0, 0, 0, 0, 0);

  man_value = 100;
  king_value = 300;

function evalute: integer;

implementation

function evalute: integer;
var
  score, i: integer;
  nwm, nbm, nwk, nbk: byte;
  lwm, lbm, lwk, lbk: byte;
  rwm, rbm, rwk, rbk: byte;
begin
  if b_man = 0 then
  begin
    if wtm = 0 then
      Result := inf
    else
      Result := -inf;
    exit;
  end;
  if w_man = 0 then
  begin
    if wtm = 0 then
      Result := -inf
    else
      Result := inf;
    exit;
  end;
  score := 0;
  lwm := 0;
  lbm := 0;
  lwk := 0;
  lbk := 0;
  rwm := 0;
  rbm := 0;
  rwk := 0;
  rbk := 0;
  for i := 5 to 39 do
    if (board[i] and white) <> 0 then
    begin
      if (board[i] and king) <> 0 then
      begin
        if left[i] = 1 then
          Inc(lwk)
        else
          Inc(rwk);
        Inc(score, king_value + pst_king[i]);
      end
      else
      begin
        if left[i] = 1 then
          Inc(lwm)
        else
          Inc(rwm);
        Inc(score, man_value + pst_man[i]);
      end;
    end
    else if (board[i] and black) <> 0 then
    begin
      if (board[i] and king) <> 0 then
      begin
        if left[i] = 1 then
          Inc(lbk)
        else
          Inc(rbk);
        Dec(score, king_value + pst_king[i]);
      end
      else
      begin
        if left[i] = 1 then
          Inc(lbm)
        else
          Inc(rbm);
        Dec(score, man_value + pst_man[44 - i]);
      end;
    end;
  nwk := lwk + rwk;
  nwm := lwm + rwm;
  nbk := lbk + rbk;
  nbm := lbm + rbm;
  if (nwm = 0) and (nbm = 0) then
    if (abs(nbk - nwk) = 1) or (nbk - nwk = 0) then
    begin
      Result := 0;
      exit;
    end;
  if wtm = 0 then
    Inc(score, 3)
  else
    Dec(score, 3);
  if (nbm = nwm) and (nbk = 0) and (nwk = 0) then
  begin
    Dec(score, 2 * abs(rwm - lwm));
    Inc(score, 2 * abs(rbm - lbm));
  end;
  if wtm = 0 then
    Result := score
  else
    Result := -score;
end;

end.
