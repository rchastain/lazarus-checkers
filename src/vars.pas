unit vars;

{$MODE Delphi}

interface

const
  ver = 'FreeCheckers v 1.04';

  startpos = 'bbbbbbbbbbbb........wwwwwwwwwwwww';

  maxply = 99;

  inf = 10000;

  white = 1;
  black = 2;
  king = 4;
  change_color = 3;
  white_man = white;
  white_king = white or king;
  black_man = black;
  black_king = black or king;

  koord: array[0..44] of string[2] =
    ('', '', '', '', '',
    'b8', 'd8', 'f8', 'h8',
    'a7', 'c7', 'e7', 'g7', '',
    'b6', 'd6', 'f6', 'h6',
    'a5', 'c5', 'e5', 'g5', '',
    'b4', 'd4', 'f4', 'h4',
    'a3', 'c3', 'e3', 'g3', '',
    'b2', 'd2', 'f2', 'h2',
    'a1', 'c1', 'e1', 'g1', '',
    '', '', '', '');

type
  TMove = record
    m_from, m_to: byte;
    prom: boolean;
    cap_sq: array[1..12] of byte;
    cap_type: array[1..12] of byte;
    cap_num: byte;
  end;
  TBoard = array[0..44] of byte;
  TLine = array[0..maxply + 1] of tmove;

var
  board: TBoard;
  wtm, ctm: byte;
  moves: array[0..100 * maxply] of TMove;
  moves_num: array[0..maxply] of integer;
  moves_sort: array[0..100 * maxply] of integer;
  EndTim, Tim, Time_all, Time_inc: longint;
  rules_tim: integer;
  rules_ply: byte;
  rules_type: byte;
  nodes: longint;
  w_man, b_man: byte;
  boardkey: int64;
  stop: boolean;
  analiz: boolean;
  dumaem: boolean;
  full_mode: boolean;
  random_open: boolean;
  opens: array[1..461] of string;
  curdir: string;
  user_name: string;

implementation

end.
