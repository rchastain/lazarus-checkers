unit position;

{$MODE Delphi}

{  Board representation:

              5   6   7   8
            9  10  11  12
             14  15  16  17
           18  19  20  21
             23  24  25  26
           27  28  29  30
             32  33  34  35
           36  37  38  39


"b8" - 5, "d8" - 6, ..., "e1" - 38, "g1" - 39}

interface

uses vars, hash;

const
  stolb: array[1..8, 1..4] of byte =
    ((9, 18, 27, 36), (5, 14, 23, 32),
    (10, 19, 28, 37), (6, 15, 24, 33),
    (11, 20, 29, 38), (7, 16, 25, 34),
    (12, 21, 30, 39), (8, 17, 26, 35));

  map: array[1..32] of byte =
    (5, 6, 7, 8, 9, 10, 11, 12,
    14, 15, 16, 17, 18, 19, 20, 21,
    23, 24, 25, 26, 27, 28, 29, 30,
    32, 33, 34, 35, 36, 37, 38, 39);
  square39: array[0..63] of byte =
    (0, 5, 0, 6, 0, 7, 0, 8,
    9, 0, 10, 0, 11, 0, 12, 0,
    0, 14, 0, 15, 0, 16, 0, 17,
    18, 0, 19, 0, 20, 0, 21, 0,
    0, 23, 0, 24, 0, 25, 0, 26,
    27, 0, 28, 0, 29, 0, 30, 0,
    0, 32, 0, 33, 0, 34, 0, 35,
    36, 0, 37, 0, 38, 0, 39, 0);
  square64: array[5..39] of byte =
    (02, 04, 06, 08,
    09, 11, 13, 15, 0,
    18, 20, 22, 24,
    25, 27, 29, 31, 0,
    34, 36, 38, 40,
    41, 43, 45, 47, 0,
    50, 52, 54, 56,
    57, 59, 61, 63);

procedure printpos;
procedure newgame;
procedure strtopos(str: string);
function postostr: string;

implementation

function postostr: string;
var
  i: integer;
  s: string;
begin
  s := '';
  for i := 1 to 32 do
  begin
    case board[map[i]] of
      0: s := s + '.';
      white_man: s := s + 'w';
      black_man: s := s + 'b';
      white_king: s := s + 'W';
      black_king: s := s + 'B';
    end;
  end;
  if wtm = 0 then
    s := s + 'w'
  else
    s := s + 'b';
  Result := s;
end;

procedure printpos;
var
  x, y, sq: integer;
begin
  for x := 0 to 7 do
  begin
    for y := 0 to 7 do
    begin
      sq := square39[x * 8 + y];
      if sq <> 0 then
      begin
        case board[sq] of
          0: Write('.');
          white_man: Write('w');
          white_king: Write('W');
          black_man: Write('b');
          black_king: Write('B');
        end;
      end
      else
        Write(' ');
    end;
    writeln;
  end;
end;

procedure newgame;
begin
  strtopos(startpos);
end;

procedure strtopos(str: string);
var
  i: integer;
begin
  w_man := 0;
  b_man := 0;
  for i := 0 to 44 do
    board[i] := 0;
  board[0] := 128;
  board[1] := 128;
  board[2] := 128;
  board[3] := 128;
  board[4] := 128;
  board[13] := 128;
  board[22] := 128;
  board[31] := 128;
  board[40] := 128;
  board[41] := 128;
  board[42] := 128;
  board[43] := 128;
  board[44] := 128;
  for i := 1 to 32 do
    case str[i] of
      '.': board[map[i]] := 0;
      'w':
      begin
        board[map[i]] := white_man;
        Inc(w_man);
      end;
      'W':
      begin
        board[map[i]] := white_king;
        Inc(w_man);
      end;
      'b':
      begin
        board[map[i]] := black_man;
        Inc(b_man);
      end;
      'B':
      begin
        board[map[i]] := black_king;
        Inc(b_man);
      end;
    end;
  if str[33] = 'w' then
    wtm := 0
  else
    wtm := 1;
  boardkey := GetPositionHash;
end;

end.
