unit hash;

{$MODE Delphi}

interface

uses vars;

type
  TTrans = record
    Key: int64;
    Value: smallint;
    depth: byte;
    flag: byte;
    move_from: byte;
    move_to: byte;
  end;
  TZobrist = array[1..4, 5..39] of int64;

const
  HExact = 0;
  HUpper = 1;
  HLower = 2;

var
  Zobrist: TZobrist;
  ZColor: int64;
  TransTable: array of TTrans;
  HashSizeMB, HashSize: integer;

  HashMove_from: byte;
  HashMove_to: byte;
  HashValue: smallint;
  HashFlag: byte;
  HashDepth: byte;

procedure FillZobristTables;
function GetRandom: int64;
function GetPositionHash: int64;
procedure ClearHash;
procedure HashInit;
procedure SaveToHash;
procedure HashProbe;

implementation

procedure FillZobristTables;
var
  i, j: integer;
begin
  randomize;
  for i := 1 to 4 do
    for j := 5 to 39 do
      Zobrist[i, j] := GetRandom;
  ZColor := GetRandom;
end;

function GetRandom: int64;
var
  res: int64;
  t1, t2, t3, t4: word;
  temp1, temp2: longword;
begin
  t1 := Random(65536);
  t2 := Random(65536);
  t3 := Random(65536);
  t4 := Random(65536);
  temp1 := (t1 shl 16) xor t2;
  temp2 := (t3 shl 16) xor t4;
  res := (temp1 shl 32) xor temp2;
  Result := res;
end;

function GetPositionHash: int64;
var
  res: int64;
  i: byte;
begin
  res := 0;
  for i := 5 to 39 do
    if (board[i] and 7) <> 0 then
      res := res xor (Zobrist[board[i], i]);
  if wtm = 1 then
    res := res xor ZColor;
  Result := res;
end;

procedure ClearHash;
var
  i: integer;
begin
  for i := 0 to HashSize do
  begin
    TransTable[i].Key := 0;
    TransTable[i].Value := 0;
    TransTable[i].depth := 0;
    TransTable[i].flag := 255;
    TransTable[i].move_from := 0;
    TransTable[i].move_to := 0;
  end;
end;

procedure HashInit;
begin
  HashSize := trunc(HashSizeMB * 1024 * 1024 / sizeof(TTrans));
  SetLength(TransTable, HashSize);
  ClearHash;
end;

procedure SaveToHash;
var
  ind: integer;
begin
  ind := Abs(boardkey mod HashSize);
  if TransTable[ind].depth <= HashDepth then
  begin
    TransTable[ind].Key := boardkey;
    TransTable[ind].Value := HashValue;
    TransTable[ind].depth := HashDepth;
    TransTable[ind].flag := HashFlag;
    TransTable[ind].move_from := HashMove_from;
    TransTable[ind].move_to := HashMove_to;
  end;
end;

procedure HashProbe;
var
  ind: integer;
begin
  ind := Abs(boardkey mod HashSize);
  if TransTable[ind].Key = boardkey then
  begin
    HashMove_from := TransTable[ind].move_from;
    HashMove_to := TransTable[ind].move_to;
    HashValue := TransTable[ind].Value;
    HashFlag := TransTable[ind].flag;
    HashDepth := TransTable[ind].depth;
  end
  else
  begin
    HashMove_from := 0;
    HashMove_to := 0;
    HashValue := 0;
    HashFlag := 255;
    HashDepth := 0;
  end;
end;

end.
