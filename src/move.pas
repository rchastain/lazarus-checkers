unit move;

{$MODE Delphi}

interface

uses vars, hash;

procedure move_do(mov: tmove);
procedure move_undo(mov: tmove);

implementation

procedure move_do(mov: tmove);
var
  i: integer;
begin
  boardkey := boardkey xor zobrist[board[mov.m_from], mov.m_from];
  if mov.prom then
    board[mov.m_to] := board[mov.m_from] or king
  else
    board[mov.m_to] := board[mov.m_from];
  boardkey := boardkey xor zobrist[board[mov.m_to], mov.m_to];
  if mov.m_from <> mov.m_to then
    board[mov.m_from] := 0;
  for i := 1 to mov.cap_num do
  begin
    boardkey := boardkey xor zobrist[board[mov.cap_sq[i]], mov.cap_sq[i]];
    board[mov.cap_sq[i]] := 0;
  end;
  if wtm = 0 then
    Dec(b_man, mov.cap_num)
  else
    Dec(w_man, mov.cap_num);
  wtm := wtm xor 1;
  boardkey := boardkey xor zcolor;
end;

procedure move_undo(mov: tmove);
var
  i: integer;
begin
  wtm := wtm xor 1;
  if wtm = 0 then
    Inc(b_man, mov.cap_num)
  else
    Inc(w_man, mov.cap_num);
  for i := 1 to mov.cap_num do
    board[mov.cap_sq[i]] := mov.cap_type[i];
  if mov.prom then
    board[mov.m_from] := wtm + 1
  else
    board[mov.m_from] := board[mov.m_to];
  if mov.m_from <> mov.m_to then
    board[mov.m_to] := 0;
end;

end.
