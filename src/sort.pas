unit sort;

{$MODE Delphi}

interface

uses vars, eval, history, hash;

procedure movesort(f, t: integer);
procedure SimpSort(ply: integer);
procedure CapSort(ply: integer);

implementation

procedure SimpSort(ply: integer);
var
  i, f, t: integer;
begin
  f := 100 * ply + 1;
  t := moves_num[ply];
  for i := f to t do
  begin
    if (moves[i].m_from = hashmove_from) and (moves[i].m_to = hashmove_to) then
      moves_sort[i] := HistoryMax + 5
    else if (moves[i].m_from = killer[ply, 1, 1]) and (moves[i].m_to = killer[ply, 1, 2]) then
      moves_sort[i] := HistoryMax + 2
    else if (moves[i].m_from = killer[ply, 2, 1]) and (moves[i].m_to = killer[ply, 2, 2]) then
      moves_sort[i] := HistoryMax + 1
    else
      moves_sort[i] := HistoryScore(moves[i]);
  end;
  if f < t then
    movesort(f, t);
end;

procedure CapSort(ply: integer);
var
  i, f, t: integer;
begin
  f := 100 * ply + 1;
  t := moves_num[ply];
  for i := f to t do
    moves_sort[i] := 100 * moves[i].cap_num;
  if f < t then
    movesort(f, t);
end;

procedure movesort(f, t: integer);
var
  i, j: integer;
  move: tmove;
  Value: integer;
begin
  moves_sort[t + 1] := -32768;
  for i := t - 1 downto f do
  begin
    move := moves[i];
    Value := moves_sort[i];
    j := i;
    while (Value < moves_sort[j + 1]) do
    begin
      moves[j] := moves[j + 1];
      moves_sort[j] := moves_sort[j + 1];
      Inc(j);
    end;
    moves[j] := move;
    moves_sort[j] := Value;
  end;
end;

end.
