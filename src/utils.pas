unit utils;

{$MODE Delphi}

interface

uses vars, position, generator, SysUtils;

var
  StartTime1, CurrTime1: TDateTime;
  StartTime2, CurrTime2: TDateTime;

procedure StartTimer1;
function GetTimer1: longint;
procedure StartTimer2;
function GetTimer2: longint;
procedure test1;
function movetostr(mov: tmove): string;
function move3str(mov: tmove): string;
function strtomove(str: string; var mov: tmove): boolean;
function substr(var str: string; ch: char): string;

implementation

function substr(var str: string; ch: char): string;
var
  a: integer;
begin
  a := pos(ch, str);
  if a = 0 then
  begin
    Result := str;
    str := '';
  end
  else
  begin
    Result := copy(str, 1, a - 1);
    str := copy(str, a + 1, length(str) - a);
  end;
end;

function move3str(mov: tmove): string;
var
  s: string;
begin
  s := koord[mov.m_from];
  if mov.cap_num <> 0 then
    s := s + ':';
  s := s + koord[mov.m_to];
  Result := s;
end;

function strtomove(str: string; var mov: tmove): boolean;
var
  i: integer;
begin
  AllGen(0);
  Result := False;
  for i := 1 to moves_num[0] do
    if str = movetostr(moves[i]) then
    begin
      Result := True;
      mov := moves[i];
      exit;
    end;
end;

function movetostr(mov: tmove): string;
var
  s: string;
  i: integer;
begin
  s := koord[mov.m_from];
  if mov.cap_num <> 0 then
    s := s + ':';
  for i := 1 to mov.cap_num do
    s := s + koord[mov.cap_sq[i]] + ':';
  s := s + koord[mov.m_to];
  Result := s;
end;

procedure test1;
var
  i: integer;
  tim: longint;
  testnodes: int64;
begin
  testnodes := 2000000;
  strtopos(startpos);
  printpos;
  StartTimer1;
  for i := 1 to testnodes do
  begin
    AllGen(0);
  end;
  Tim := GetTimer1;
  writeln('Nodes=' + IntToStr(testnodes) + ' Time=' + floattostr(tim / 1000) +
    ' NPS=' + floattostr(1000 * (testnodes / tim)));
end;

procedure StartTimer1;
begin
  StartTime1 := now;
end;

function GetTimer1: longint;
begin
  CurrTime1 := now;
  Result := trunc((CurrTime1 - StartTime1) * 86400000);
end;

procedure StartTimer2;
begin
  StartTime2 := now;
end;

function GetTimer2: longint;
begin
  CurrTime2 := now;
  Result := trunc((CurrTime2 - StartTime2) * 86400000);
end;

end.
