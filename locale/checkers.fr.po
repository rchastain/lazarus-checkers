msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fr\n"
"X-Generator: Poedit 2.4.2\n"

#: frmmain.rsgame
msgid "Партия:"
msgstr "Partie:"

#: tform1.bitbtn1.caption
msgctxt "tform1.bitbtn1.caption"
msgid "Новая партия"
msgstr "Nouvelle partie"

#: tform1.bitbtn10.caption
msgid "Анализ"
msgstr "Analyse"

#: tform1.bitbtn11.caption
msgid "Ход компа"
msgstr "Coup de l'ordinateur"

#: tform1.bitbtn12.caption
msgid "Стоп"
msgstr "Arrêter"

#: tform1.bitbtn13.caption
msgid "О программе"
msgstr "À propos du programme"

#: tform1.bitbtn14.caption
msgid "Выход"
msgstr "Quitter"

#: tform1.bitbtn2.caption
msgid "Открыть"
msgstr "Ouvrir"

#: tform1.bitbtn3.caption
msgid "Сохранить"
msgstr "Sauvegarder"

#: tform1.bitbtn4.caption
msgid "Повернуть доску"
msgstr "Tourner le damier"

#: tform1.bitbtn5.caption
msgid "<"
msgstr ""

#: tform1.bitbtn6.caption
msgid ">"
msgstr ""

#: tform1.bitbtn7.caption
msgid "<<<"
msgstr ""

#: tform1.bitbtn8.caption
msgid ">>>"
msgstr ""

#: tform1.bitbtn9.caption
msgid "Показать Output"
msgstr "Affichage sortie moteur"

#: tform4.bitbtn2.caption
msgctxt "tform4.bitbtn2.caption"
msgid "Отмена"
msgstr "Annuler"

#: tform4.caption
msgctxt "tform4.caption"
msgid "Новая партия"
msgstr "Nouvelle partie"

#: tform4.checkbox1.caption
msgctxt "tform4.checkbox1.caption"
msgid "Случайный дебют"
msgstr "Position de départ aléatoire"

#: tform4.edit1.text
msgctxt "tform4.edit1.text"
msgid "Юзер Юзерович"
msgstr "Utilisateur"

#: tform4.groupbox2.caption
msgctxt "tform4.groupbox2.caption"
msgid "Контроль:"
msgstr "Contrôle :"

#: tform4.label1.caption
msgctxt "tform4.label1.caption"
msgid "Фамилия, Имя:"
msgstr "Nom de l'utilisateur :"

#: tform4.radiobutton1.caption
msgctxt "tform4.radiobutton1.caption"
msgid "Компьютер играет черными"
msgstr "L'ordinateur joue les noirs"

#: tform4.radiobutton2.caption
msgctxt "tform4.radiobutton2.caption"
msgid "Компьютер играет белыми"
msgstr "L'ordinateur joue les blancs"

#: tform4.radiobutton3.caption
msgctxt "tform4.radiobutton3.caption"
msgid "Компьютер не играет"
msgstr "L'ordinateur ne joue pas"

#: tform4.radiobutton4.caption
msgctxt "tform4.radiobutton4.caption"
msgid "Время:"
msgstr "Temps :"

#: tform4.radiobutton5.caption
msgctxt "tform4.radiobutton5.caption"
msgid "Глубина:"
msgstr "Profondeur :"
